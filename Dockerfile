# I used the example Dockerfile at https://nodejs.org/en/docs/guides/nodejs-docker-webapp/ as a template for this.
# Pull base image.
FROM node:14

# Create app directory
WORKDIR /usr/src/app

# Bundle app source
COPY ./src /usr/src/app/src
COPY ./public /usr/src/app/public

# Install app dependencies
COPY package.json /usr/src/app 

# Create app directory
#WORKDIR /usr/src/app
RUN ls /usr/src/app

RUN npm install
RUN npm start


# Bundle app source
#COPY ./src /usr/src/app
#COPY ./public /usr/src/app

# Expose ports.
EXPOSE 8080

# Start the service
# CMD [ "npm start" ]

# Start the service
#CMD [ "npm start" ]
#CMD [ "nodejs src/index.js" ]

#############

# FROM nginx:latest

