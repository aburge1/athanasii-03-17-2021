I used https://docs.tilt.dev/tiltfile_concepts.html as a guide to build my Tiltfile.
---

I used https://docs.tilt.dev/example_nodejs.html as a guide to build athanasii-03-17-202.yaml.
---

I found 1 high severity vulnerability:  run `npm audit fix` to fix them, or `npm audit` for details
---


I used the example at https://blog.morizyun.com/javascript/gitlab-ci-gitlab-ci-yml.html for the .gitlab-ci.yml.
---


Emoji Search is not my work.  It is cloned from https://github.com/ahfarmer/emoji-search
---

Created with *create-react-app*. See the [full create-react-app guide](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).



Install
---

`npm install`



Usage
---

`npm start`
